pub fn rdmsr(msr: u32) u64 {
    var eax = 0;
    var edx = 0;
    asm volatile("rdmsr"
    : [eax] "={eax}" (eax),
      [edx] "={edx}" (edx)
    : [msr] "{ecx}" (msr),
    : "memory");

    return (edx << 32) | eax;
}

pub fn wrmsr(msr: u32, value: u64) void {
    var eax: u32 = value;
    var edx = value >> 32;
    asm volatile("wrmsr"
    : : [eax] "{eax}" (eax),
        [edx] "{edx}" (edx),
        [msr] "{ecx}" (msr),
    : "memory");
}