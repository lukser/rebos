const lock = @import("../lib/lock.zig");

// Segment selectors
const Segment = struct {
    const kernelCode = 0x08;
    const kernelData = 0x10;
    const userCode = 0x18;
    const userData = 0x20;
    const tss = 0x48;
};

// Access bytes
const Access = struct {
    const kernel = 0x90;
    const user = 0xf0;
    const code = 0x0a;
    const data = 0x02;
    const tss = 0x89;
};

// Segment flags
const SegmentFlags = struct {
    const protected = 1 << 2;
    const blocks4k = 1 << 3;
};

const GDTRegister = packed struct {
    limit: u16,
    base: *const GDTEntry,
};

const GDTEntry = packed struct {
    limit_low: u16,
    base_low: u16,
    base_mid: u8,
    access: u8,
    limit_high: u4,
    flags: u4,
    base_high: u8,

    fn init(
        base: usize,
        limit: usize,
        access: u8,
        flags: u4
    ) GDTEntry {
        return GDTEntry{
            .limit_low = @truncate(u16, limit),
            .base_low = @truncate(u16, base),
            .base_mid = @truncate(u8, base >> 16),
            .access = @truncate(u8, access),
            .limit_high = @truncate(u4, limit >> 16),
            .flags = @truncate(u4, flags),
            .base_high = @truncate(u8, base >> 24),
        };
    }
};

const TaskStateSegment = packed struct {
    unused1: u32,
    esp0: u32, // Stack, when ring(>0) to ring0
    ss0: u32, // Segment, when ring(>0) to ring0
    unused2: [22]u32,
    unused3: u16,
    iomap_base: u16, // base of the IO bitmap
};

var gdtr: GDTRegister = undefined;
var gdt: []GDTEntry align(4) = undefined;
var gdtLock: lock.Lock = .{
    .l = undefined,
    .caller = undefined,
};

pub fn initialize() void {
    gdt = &[_]GDTEntry{
        GDTEntry.init(0, 0, 0, 0), // Null descriptor

        GDTEntry.init(0, 0xffff, Access.kernel | Access.code, 0), // Ring 0 16-bit code
        GDTEntry.init(0, 0xffff, Access.kernel | Access.data, 0), // Ring 0 16-bit data
        GDTEntry.init(0, 0xffff, Access.kernel | Access.code, SegmentFlags.protected | SegmentFlags.blocks4k), // Ring 0 32-bit code
        GDTEntry.init(0, 0xffff, Access.kernel | Access.data, SegmentFlags.protected | SegmentFlags.blocks4k), // Ring 0 32-bit data

        GDTEntry.init(0, 0xffff, Access.kernel | Access.code, SegmentFlags.protected | SegmentFlags.blocks4k), // Kernel 64-bit code
        GDTEntry.init(0, 0xffff, Access.kernel | Access.data, SegmentFlags.protected | SegmentFlags.blocks4k), // Kernel 64-bit data
        GDTEntry.init(0, 0xffff, Access.user | Access.code, SegmentFlags.protected | SegmentFlags.blocks4k), // User 64-bit code
        GDTEntry.init(0, 0xffff, Access.user | Access.data, SegmentFlags.protected | SegmentFlags.blocks4k), // User 64-bit data

        GDTEntry.init(0, 0, 0, 0), // TSS, fill at runtime
    };

    reload();
}

pub fn load_tss(addr: *usize) void {
    gdtLock.acquire();
    gdt[gdt.len - 1] = GDTEntry.init(addr, 0x67, Access.tss, 0);

    asm volatile("ltr %[offset]"
    : : [offset] "{eax}" (Segment.tss)
    : "memory");
    gdtLock.release();
}

pub fn reload() void {
    gdtr = GDTRegister{
        .limit = @as(u16, @sizeOf(@TypeOf(gdt))),
        .base = &gdt[0],
    };

    asm volatile(
        \\lgdt %[ptr]
        \\push rax
        \\push %[cseg]
        \\lea rax, [rip + 0x03]
        \\push rax
        \\retfq
        \\pop rax
        \\mov ds, %[dseg]
        \\mov es, %[dseg]
        \\mov ss, %[dseg]
        \\mov fs, %[udseg]
        \\mov gs, %[udseg]
    : : [cseg] "{ax}" (Segment.kernelCode),
        [dseg] "{ax}" (Segment.kernelData),
        [udseg] "{ax}" (Segment.userData),
        [ptr] "*{eax}" (gdtr),
    : "memory");
}