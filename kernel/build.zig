const std = @import("std");

pub fn build(b: *std.build.Builder) !void {
    // Define a freestanding x86_64 cross-compilation target.
    var target: std.zig.CrossTarget = .{
        .cpu_arch = .x86_64,
        .os_tag = .freestanding,
        .abi = .none,
    };
    const mode = b.standardReleaseOptions();

    // Disable CPU features that require additional initialization
    // like MMX, SSE/2 and AVX. That requires us to enable the soft-float feature.
    const Features = std.Target.x86.Feature;
    target.cpu_features_sub.addFeature(@enumToInt(Features.mmx));
    target.cpu_features_sub.addFeature(@enumToInt(Features.sse));
    target.cpu_features_sub.addFeature(@enumToInt(Features.sse2));
    target.cpu_features_sub.addFeature(@enumToInt(Features.avx));
    target.cpu_features_sub.addFeature(@enumToInt(Features.avx2));
    target.cpu_features_add.addFeature(@enumToInt(Features.soft_float));

    // Build the kernel itself.
    const kernel = b.addExecutable("kernel.elf", null);
    kernel.code_model = .kernel;
    kernel.setBuildMode(mode);

    kernel.addCSourceFiles(&.{
        "src/kernel.c",
        "src/memory.c",
        "src/stdio.c",
        "src/string.c",
        "src/system.c",

        "src/arch/x86/gdt.c",
        "src/arch/x86/registers.c",
    }, &.{
        "-g",
        "-O2",
        "-pipe",
        "-Wall",
        "-Wextra",
    });

    kernel.addIncludePath("include");
    kernel.setLinkerScriptPath(.{ .path = "linker.ld" });
    kernel.setTarget(target);
    kernel.install();
}
