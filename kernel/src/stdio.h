//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#ifndef REBOS_STDIO_H
#define REBOS_STDIO_H

#include <stdint.h>

uint8_t inportb(uint16_t port);

void outportb(uint16_t port, uint8_t data);

struct limine_terminal *terminal_get();

void print(const char *text);

#endif //REBOS_STDIO_H
