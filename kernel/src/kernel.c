//
// Created by Sergei Lukaushkin on 06.01.2023.
//
#include "stdio.h"
#include "system.h"
#include "arch/x86/gdt.h"


void _start(void) {
    gdt_init();
    print("Hello, World");
    halt();
}