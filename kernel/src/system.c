//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#include "system.h"
#include "stdio.h"

_Noreturn static void halt(void) {
    for (;;) {
        __asm__("hlt");
    }
}

void panic(unsigned char *msg) {
    print("\n");
    print(msg);
    print("\n");
    for (;;);
}

