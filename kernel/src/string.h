//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#ifndef REBOS_STRING_H
#define REBOS_STRING_H

#include <stdint.h>
#include <stddef.h>

int32_t strlen(const unsigned char *str);

int32_t strcpy(unsigned char *dest, const unsigned char *str, size_t size);

int32_t strcmp(const unsigned char *a, const unsigned char *b);

unsigned char *strrev(unsigned char *str);

unsigned char *itoa(uint64_t num, unsigned char *str, int32_t base);

#endif //REBOS_STRING_H
