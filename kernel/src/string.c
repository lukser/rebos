//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#include "string.h"

/*
 * Determine string length.
 */
int32_t strlen(const unsigned char *str) {
    const unsigned char *s;
    for (s = str; *s; ++s);
    return (s - str);
}

/*
 * Copy string to other destination.
 */
int32_t strcpy(unsigned char *dest, const unsigned char *src, size_t size) {
    memcpy()
}

int32_t strcmp(const unsigned char *a, const unsigned char *b) {}

unsigned char *strrev(unsigned char *str) {}

unsigned char *itoa(uint64_t num, unsigned char *str, int32_t base) {}