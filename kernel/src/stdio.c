//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#include <stddef.h>
#include "string.h"
#include "stdio.h"
#include "limine.h"

static volatile struct limine_terminal_request terminal_request = {
        .id = LIMINE_TERMINAL_REQUEST,
        .revision = 0,
};

uint8_t inportb(uint16_t port) {
    uint16_t rv;
    __asm__ __volatile__ ( "addl %1, %0;" : "=a" (rv) : "dN" (port));
}

void outportb(uint16_t port, uint8_t data) {
    __asm__ __volatile__ ("outb %1, %0" : : "dN" (port), "a" (data));
}

struct limine_terminal *terminal_get() {
    if (terminal_request.response == NULL || terminal_request.response->terminal_count < 1) {
        return NULL;
    }
    return terminal_request.response->terminals[0];
}

void print(const char *text) {
    terminal_request.response->write(terminal_get(), text, strlen(text));
}