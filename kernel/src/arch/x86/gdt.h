#ifndef REBOS_ARCH_X86_GDT_H
#define REBOS_ARCH_X86_GDT_H

#include <stddef.h>
#include <stdint.h>
#include "config.h"

#define GRAN_64_BIT_MODE (1 << 5)
#define GRAN_32_BIT_MODE (1 << 6)
#define GRAN_4KIB_BLOCKS (1 << 7)

#define GDT_ENTRY(base, limit, access, granularity)                          \
	{                                                                        \
		(limit) & 0xFFFF,								   /* limit_low */   \
			(uint16_t)((base) >> 0 & 0xFFFF),			   /* base_low */    \
			(uint8_t)((base) >> 16 & 0xFF),				   /* base_mid */    \
			(access)&0xFF,								   /* access */      \
			((limit) >> 16 & 0x0F) | ((granularity)&0xF0), /* granularity */ \
			(uint8_t)((base) >> 24 & 0xFF),				   /* base_high */   \
	}

#define GDT_ENTRY64(base, limit, access, granularity)                    \
	{                                                                    \
		(limit)&0xFFFF,								   /* limit_low */   \
		(uint16_t)((base) >> 0 & 0xFFFF),			   /* base_low */    \
		(uint8_t)((base) >> 16 & 0xFF),				   /* base_mid */    \
		(access)&0xFF,								   /* access */      \
		((limit) >> 16 & 0x0F) | ((granularity)&0xF0), /* granularity */ \
		(uint8_t)((base) >> 24 & 0xFF),				   /* base_high */   \
	},                                                                   \
	{                                                                    \
		(uint16_t)((base) >> 32 & 0xFFFF),	   /* base_highest */        \
			(uint16_t)((base) >> 48 & 0xFFFF), /* base_highest */        \
			0,								   /* reserved0 */           \
			0,								   /* reserved0 */           \
			0,								   /* reserved0 */           \
			0,								   /* reserved0 */           \
	}

struct gdt_entry
{
	uint16_t limit_low;
	uint16_t base_low;
	uint8_t base_mid;
	uint8_t access;
	uint8_t granularity;
	uint8_t base_high;
} __attribute__((packed));

struct gdt_entry64
{
	uint16_t limit_low;
	uint16_t base_low;
	uint8_t base_mid;
	uint8_t access;
	uint8_t granularity;
	uint8_t base_high;
	uint32_t base_highest;
	uint32_t reserved0;
} __attribute__((packed));

struct tss_entry
{
	uint32_t prev_tss;
	uint32_t esp0;
	uint32_t ss0;
	uint32_t esp1;
	uint32_t ss1;
	uint32_t esp2;
	uint32_t ss2;
	uint32_t cr3;
	uint32_t eip;
	uint32_t eflags;
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;
	uint32_t esi;
	uint32_t edi;
	uint32_t es;
	uint32_t cs;
	uint32_t ss;
	uint32_t ds;
	uint32_t fs;
	uint32_t gs;
	uint32_t ldt;
	uint16_t trap;
	uint16_t iomap_base;
} __attribute__((packed));

struct tss_entry64
{
	uint32_t reserved0;
	uint64_t stack0;
	uint64_t stack1;
	uint64_t stack2;
	uint64_t reserved1;
	uint64_t ist[7];
	uint64_t reserved2;
	uint16_t reserved3;
	uint16_t iomap_base;
} __attribute__((packed));

#if defined(__i386__)
typedef struct tss_entry tss_t;
typedef struct gdt_entry gdt_t;
#elif defined(__x86_64__)
typedef struct tss_entry64 tss_t;
typedef struct gdt_entry64 gdt_t;
#endif

const size_t STACK_SIZE = 64 * 1024;
extern size_t stack[STACK_SIZE / sizeof(size_t)];

tss_t g_tss;
gdt_t g_gdt[];

uintptr_t gdt_get_kernel_stack();
void gdt_set_kernel_stack(uintptr_t stack_pointer);

#if defined(__i386__)
uint32_t gdt_get_fs_base();
void gdt_set_fs_base(uint32_t fsbase);

uint32_t gdt_get_gs_base();
void gdt_set_gs_base(uint32_t gsbase);
#endif

#endif // REBOS_ARCH_X86_GDT_H