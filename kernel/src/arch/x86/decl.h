#ifndef REBOS_ARCH_X86_DECL_H
#define REBOS_ARCH_X86_DECL_H

#include <stdint.h>

typedef uintptr_t addr_t;

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

#if !defined(CPU) && defined(__i386__)
#define CPU X86
#endif

#if !defined(CPU) && defined(__X86_64__)
#define CPU X64
#endif

#endif // REBOS_ARCH_X86_DECL_H