#include "registers.h"

static inline bool intctx_in_userspace(const intctx_t *intctx) {
    return (intctx->cs & RPLMASK) != KRPL;
}

void intctx_log(const intctx_t *intctx) {
#if defined(__i386__)
#elif defined(__x86_64)
#endif
}

void load_registers(const threg_t *registers) {}
