#include <assert.h>
#include "registers.h"
#include "gdt.h"

tss_t tss = {
#if defined(__i386__)
    .prev_tss = 0,
    .esp0 = 0, // (uintptr_t) stack + sizeof(stack)
    .ss0 = 0x10, // Kernel Data Segment,
    .esp1 = 0,
    .ss1 = 0,                                                          /* c++ */
	.esp2 = 0,                                                         /* c++ */
	.ss2 = 0,                                                          /* c++ */
	.cr3 = 0,                                                          /* c++ */
	.eip = 0,                                                          /* c++ */
	.eflags = 0,                                                       /* c++ */
	.eax = 0,                                                          /* c++ */
	.ecx = 0,                                                          /* c++ */
	.edx = 0,                                                          /* c++ */
	.ebx = 0,                                                          /* c++ */
	.esp = 0,                                                          /* c++ */
	.ebp = 0,                                                          /* c++ */
	.esi = 0,                                                          /* c++ */
	.edi = 0,
    .es = 0x13, // Kernel Data Segment
    .cs = 0x0B, // Kernel Code Segment
    .ss = 0,
    .ds = 0x13, // Kernel Data Segment
    .fs = 0x13, // Kernel Data Segment
    .gs = 0x13, // Kernel Data Segment
    .ldt = 0,
    .trap = 0,
    .iomap_base = 0,
#elif defined(__x86_64__)
    .reserved0 = 0,
    .stack0 = 0 /* (uintptr_t) stack + sizeof(stack) */,
    .stack1 = 0,
    .stack2 = 0,
    .reserved1 = 0,
    .ist = {0, 0, 0, 0, 0, 0, 0},
    .reserved2 = 0,
    .reserved3 = 0,
    .iomap_base = 0,
#endif
};

gdt_t gdt[] = {
    // 0x00: Null Segment
    GDT_ENTRY(0, 0, 0, 0),

#if defined(__i386__)
    // 0x08: Kernel Code Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0x9A, GRAN_32_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x10: Kernel Data Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0x92, GRAN_32_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x18: User Code Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0xFA, GRAN_32_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x20: User Data Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0xF2, GRAN_32_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x28: Task Switch Segment
    GDT_ENTRY(0 /* ((uintptr_t) &tss) */, sizeof(tss) - 1, 0xE9, 0x00),

    // 0x30: F Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0xF2, GRAN_32_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x38: G Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0xF2, GRAN_32_BIT_MODE | GRAN_4KIB_BLOCKS),
#elif defined(__x86_64__)
    // 0x08: Kernel Code Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0x9A, GRAN_64_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x10: Kernel Data Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0x92, GRAN_64_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x18: User Code Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0xFA, GRAN_64_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x20: User Data Segment
    GDT_ENTRY(0, 0xFFFFFFFF, 0xF2, GRAN_64_BIT_MODE | GRAN_4KIB_BLOCKS),

    // 0x28: Task Switch Segment
    GDT_ENTRY64((uint64_t) 0 /* ((uintptr_t) &tss) */, sizeof(tss) - 1, 0xE9, 0x00),
#endif
};

uintptr_t gdt_get_kernel_stack() {
#if defined(__i386__)
    return tss.esp0;
#elif defined(__x86_64__)
    return tss.stack0;
#endif
}

void gdt_set_kernel_stack(uintptr_t stack_pointer) {
    assert((stack_pointer & 0xF) == 0);
#if defined(__i386__)
    tss.esp0 = (uint32_t) stack_pointer;
#elif defined(__x86_64__)
    tss.stack0 = (uint64_t) stack_pointer;
#endif
}

#if defined(__i386__)
uint32_t gdt_get_fs_base() {
    gdt_t *entry = gdt + GDT_FS_ENTRY;
    return (uint32_t) entry->base_low << 0 |
           (uint32_t) entry->base_mid << 16 |
           (uint32_t) entry->base_high << 24;
}

void gdt_set_fs_base(uint32_t fsbase) {
    gdt_t *entry = gdt + GDT_FS_ENTRY;
    entry->base_low = fsbase >> 0 & 0xFFFF;
    entry->base_mid = fsbase >> 16 & 0xFF;
    entry->base_high = fsbase >> 24 & 0xFF;
    asm volatile ("mov %0, %%fs" : : "r" (GDT_FS_ENTRY << 3 | URPL));
}

uint32_t gdt_get_gs_base() {
    gdt_t *entry = gdt + GDT_GS_ENTRY;
    return (uint32_t) entry->base_low << 0 |
           (uint32_t) entry->base_mid << 16 |
           (uint32_t) entry->base_high << 24;
}

void gdt_set_gs_base(uint32_t gsbase) {
    gdt_t *entry = gdt + GDT_GS_ENTRY;
    entry->base_low = gsbase >> 0 & 0xFFFF;
    entry->base_mid = gsbase >> 16 & 0xFF;
    entry->base_high = gsbase >> 24 & 0xFF;
    asm volatile ("mov %0, %%fs" : : "r" (GDT_GS_ENTRY << 3 | URPL));
}
#endif
