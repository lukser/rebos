//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#ifndef REBOS_MEMORY_H
#define REBOS_MEMORY_H

#include <stddef.h>
#include <stdint.h>

void *memcpy(void *dest, const void *src, size_t count);

void *memset(void *dest, uint8_t val, size_t count);

uint16_t *memsetw(uint16_t *dest, uint16_t val, size_t count);

#endif //REBOS_MEMORY_H
