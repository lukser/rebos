//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#include "memory.h"

void *memcpy(void *dest, const void *src, size_t count) {
    unsigned char *destC = (unsigned char *) dest;
    unsigned char *srcC = (unsigned char *) src;
    for (size_t i = 0; i < count; i++)
        destC[i] = srcC[i];
    return dest;
}

void *memset(void *dest, uint8_t val, size_t count) {
    unsigned char *destC = (unsigned char *) dest;
    for (size_t i = 0; i < count; i++)
        destC[i] = val;
    return dest;
}

uint16_t *memsetw(uint16_t *dest, uint16_t val, size_t count) {
    for (size_t i = 0; i < count; i++)
        dest[i] = val;
    return dest;
}
