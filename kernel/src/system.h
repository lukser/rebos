//
// Created by Sergei Lukaushkin on 06.01.2023.
//

#include <stdint.h>

#ifndef REBOS_SYSTEM_H
#define REBOS_SYSTEM_H

#define __QUOTEME_(x) #x
#define __QUOTEME(x) __QUOTEME_(x)
#define ASSERT(expr) \
    if (!(expr))     \
        panic("kernel panic (" __FILE__ ":" __QUOTEME(__LINE__) ") - assert \"" __QUOTEME(expr) "\" failed");
#define PANIC(msg) \
    panic("kernel panic (" __FILE__ ":" __QUOTEME(__LINE__) ") - " msg);

typedef uint64_t addr_t;

_Noreturn static void halt(void);

void panic(unsigned char *msg);

#endif //REBOS_SYSTEM_H
